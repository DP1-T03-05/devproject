# Meeting Minutes
**Team ID:** DP1-D03-05
**Date:** 29/09/2017
**Start Time:** 12:51PM
**End Time:** 1:07PM
**Location:** Swinburne Hawthorn - EN310
**Attendees:** Endru Joshua Hosmar, Peggy Liu, Joseph Slingo,


---
# Agenda Items

+ Choose Project
	+ Decision/Outcome: Katch-it
	+ Action: All
	
+ Create Repository
	+ Decision/Outcome: Bitbucket
	+ Action: Endru
	
+ Create README.md
	+ Decision/Outcome: Stored on Bitbucket
	+ Action: Peggy
	
+ Create Basic Bug List
	+ Decision/Outcome: Trello
	+ Action: Peggy
	
+ How will we update the Bug List
	+ Decision/Outcome: Update Trello
	+ Action: All
	
+ Bug List filename/storage
	+ Decision/Outcome: Buglist in Trello
	+ Action: Peggy
	
+ Create/update .gitignore file
	+ Decision/Outcome: Upload to repositiory
	+ Action: Endru
	
+ Task management tool for project
	+ Decision/Outcome: Trello
	+ Action: All
	
+ Create Trello
	+ Decision/Outcome: Bitbucket
	+ Action: Peggy
	
+ Link Trello to Repository
	+ Decision/Outcome: Bitbucket
	+ Action: Joseph
	
+ Upload meeting actions
	+ Decision/Outcome: To Trello
	+ Action: Joseph

+ Create group
	+ Decision/Outcome: Slack
	+ Action: Peggy

+ Filename/Storage of GDD
	+ Decision/Outcome: GameDesignDocument in Bitbucket
	+ Action: All
	
+ Add GDD to Repository
	+ Decision/Outcome: Bitbucket
	+ Action: Endru
	
+ Create the GDD
	+ Decision/Outcome: Everyone
	+ Action: All, now
	
+ Store Meeting Minutes in Repo
	+ Decision/Outcome: Bitbucket
	+ Action: Joseph
	
+ Version control system for project
	+ Decision/Outcome: Git
	+ Action: All
	
+ Naming convention standards to follow for project
	+ Decision/Outcome: CamelCase
	+ Action: All

---