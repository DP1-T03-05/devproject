#Katch-It Game Design Documentation

Team Member: Peiqi liu
             Joseph Slingo
             Endru Joshua Hosmar
            
## Team and Team title

ID:DP1-T03-05
Team Member: Peiqi liu
Joseph Slingo
Endru Joshua Hosmar


##Overview of the gameplay

The player should be able to catch all the falling objects in order to get scores

##gameplay

The player can move left or right with the keyboard pressed.
There are different kinds of objects to catch, some of them contribute to the scores, some of them do not.
The game start at 0. Score will increase when player catch objects.

## User Interface

+ **Main Menu**
	The home screen of the game which is seen when you launch the program. The menu
	contains buttons to change the 'catching tool' being used (back and forth) in 
	which text changes based on which current tool has been selected.
	
+ **Playing Screen**
	The in-game screen used while playing the game. The screen contains text, such 
	as a the highest score to beat, the current score and level at which the user 
	is at. It also contains images such as the health bar, catching tool and the
	objects able to be caught.

+ **Pause Screen**
	Screen used when the game is paused mid-game. The game contains little to no
	graphics except text telling the user that the game is paused. Could possibly 
	contain a list of highscores and give the option for the user to save and exit
	in the future.

##Assets:

1)visual of the player
2)side menu bar
3)sound effect
4)health bar

## User Story

+ Task:Design different object
+ Story:1.As a player, I want to be able to catch more variety of objects

+ Task: Design Disadvantageous Object
+ Story: I want to be able to have avoidable object 

+ Task: User can increase movespeed with button
+ Story:I want to be able to boost my speed

+ Task: Sound effect
+ Story: I want to be able to hear indication of an action

+ Task:Different Image of catcher available
+ Story: I want to be able to change the visual of the catcher to my preference

+ Task: Score in-game
+ Story:I want to be able to see the score

+ Task: See high score
+ Story:I want to be able to see my highest score before starting the game

+ Task: Hit point
+ Story:I want to be able to see my health status

+ Task: Main menu
+ Story:I want to be able to have a main menu

+ Task: Able to pause
+ Story:I want to be able to pause during the game

	
