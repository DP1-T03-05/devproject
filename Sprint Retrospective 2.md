#Katch-It Sprint Retrospective 2
Team Member: Peiqi liu
             Joseph Slingo
             Endru Joshua Hosmar
			  
## What Worked Well

+ **Implementation
    We actually got some work done this time.
	
+ **List clarity**
	We know exactly what to do from the highly descriptive list we
	have created.
	
+ **Decision Making**
	Didn't spend too long on small decisions and got straight to the point.
	
+ **Hard Working**
	Put extra effort into the work.

+ **Team Spirit**
	Happy to help each other and get along well.

+ **Repository Updates**
    Repository updates as often as it should.

	
	
## Improvements

+ **Putting it into practice**
	Put off doing the actual coding.
	Improve motivation.
	
+ **Communications**
	Not as much communication between ourselves as we should outside of class. 
	Keep communication windows open to help each other.
	

+ **Time Management**
    allocate time for this task

+  **SplashKit Learning**
   do more research about splashKit resources
