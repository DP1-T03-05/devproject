using System;
using SplashKitSDK;
using sk = SplashKitSDK.SplashKit;

namespace GameOfGrades {

    public class Enemy : GameObject {

        private bool _active;
        private static Random _rnd = new Random();

        public Enemy( Game world, float y, float dx, float dy ) : base ( world ) { //input the type/enum
            Bitmap = sk.LoadBitmap( "enemy", "enemy.png" );
            X = _rnd.Next(0, 400 - Bitmap.Width);
            Y = y;
            DX = dx;
            DY = dy;
            _active = true;
            Score = 3;
            /* set score */
        }

        public Enemy( Game world ) : this ( world, _rnd.Next( -500, -100 ), 0, _rnd.Next( 1, 4 ) ) {}

        public void Move() {
            if ( Y < sk.ScreenHeight() ) {
                X += DX;
                Y += DY;
            } else {
                _active = false;
            }
        }

        public void CheckCollision() {
            if ( sk.BitmapCollision( Bitmap, X, Y, World.Player.Bitmap, World.Player.X, World.Player.Y ) ) {
                _active = false;
                World.Player.Score += Score;
                sk.PlaySoundEffect("good");
            }
        }

        public void FlagInactive() {
            if ( !_active ) {
                World.Inactive.Add( this );
            }
        }

        public override void Update() {
            if ( _active ) {
                Move();
                CheckCollision();
                FlagInactive();
            }
        }
    }
}