# README #
### What is this repository for? ###

* A simple SplashKit game designed for Development Project 1: Tools and Practices at Swinburne University

### How do I get set up? ###

* Install SplashKit - http://www.splashkit.io/guides/installation/
* `git clone https://dp1repos@bitbucket.org/dp1repos/game-of-grades.git`
* `cd game-of-grades`
* `skm dotnet run`

For more information about SplashKit check out: http://www.splashkit.io

### Who do I talk to? ###

Contact the DP1 team: dp1.swin.repos@gmail.com