using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SplashKitSDK;
using sk = SplashKitSDK.SplashKit;

namespace GameOfGrades {

    public class Game {

        private Player _player;
        private List<Enemy> _enemies;
        private List<Enemy> _inactiveEnemies;
        private int _level;

        public Game() {
            sk.OpenWindow( "Game of Grades", 500, 600 );
            sk.LoadSoundEffect("good", "collect_good.wav");
            sk.LoadSoundEffect("bad", "collect_bad.wav");
            sk.LoadSoundEffect("gameover", "gameover.wav");
            sk.LoadFont("arial-bold", "arialbd.ttf");
            sk.LoadFont("arial", "arial.ttf");
            _player = new Player( this );
            _enemies = new List<Enemy>();
            
            _inactiveEnemies = new List<Enemy>();
        }

        public void UpdateEnemies() {
            foreach ( Enemy e in _enemies ) {
                e.Update();
            }
        }

        public void RemoveInactiveEnemies() {
            foreach ( Enemy e in _inactiveEnemies ) {
                _enemies.Remove( e );
            }
        }

        public void Update() {
            sk.ProcessEvents();
            _player.Update();
            UpdateEnemies();
            RemoveInactiveEnemies();
        }

        public void RenderEnemies() {
            foreach ( Enemy e in _enemies ) {
                e.Render();
            }
        }

         public void RenderMenu(){
            Bitmap b = sk.LoadBitmap( "menu", "menu.png" );
            sk.DrawBitmap(b, 400, 0);            //render menu
        }
  
        public void RenderBar(){
            Bitmap b = sk.LoadBitmap( "healthbar", "healthbar.png" );
            //sk.DrawBitmap(b, 50,0);            //render health bar
            sk.FillRectangle(Color.White, 8, 48, 84, 19);
            sk.FillRectangle(Color.Red, 10, 50, 80, 15);
            sk.DrawText("100/100", Color.White, sk.FontNamed("arial"), 11, 30, 51.5);
        }
    
        public void Render() {
            sk.ClearScreen( sk.ColorAliceBlue() );
            _player.Render();
            RenderEnemies();
            sk.DrawText("Level " + _level, Color.Black, sk.FontNamed("arial-bold"), 16, 10, 10);
            sk.DrawText(TimerPrint(), Color.Black, sk.FontNamed("arial-bold"), 16, 350, 10);
            sk.DrawText("Score: " + _player.Score, Color.Black, sk.FontNamed("arial-bold"), 16, 10, 27);
            RenderMenu();
            RenderBar();
            sk.RefreshScreen();
        }

        public string TimerPrint()
        {
            string t = sk.TimerTicks("level").ToString();
            if (t.Length == 3)
            {
                t = String.Concat(0, ".", t[0]);
            }
            if (t.Length == 4)
            {
                t = String.Concat(t[0], ".", t[1]);
            } 
            else if (t.Length == 5)
            {
                t = String.Concat(t[0], t[1], ".", t[2]);
            }
            else if (t.Length == 6)
            {
                t = String.Concat(t[0], ":", t[1], t[2], ".", t[3]);
            }
            return t;
        }

        public void Spawn(int enemyCount)
        {
            enemyCount = enemyCount / 1000;
            for (int i = 0; i < enemyCount; i++ ) {
                _enemies.Add( new Enemy( this ) );
            }
        }

        public void Start()
        {
            Timer level = new Timer("level");
            int nextLevel = 5000;
            _level = 1;
            Spawn(2000);
            level.Start();
            while ( !sk.WindowCloseRequested( "Game of Grades" ) ) {
                if (sk.TimerTicks(level) > nextLevel)
                {
                    Spawn(nextLevel);
                    nextLevel += nextLevel/2;
                    _level++;
                }
                Update();
                Render();
                
            }
            sk.FreeAllFonts();
            sk.FreeAllBitmaps();
            sk.FreeAllSoundEffects();
        }

        public Player Player {
            get { return _player; }
        }

        public List<Enemy> Inactive {
            get { return _inactiveEnemies; }
        }

        public bool Button(float x1, float y1, float x2, float y2)
        {
            if (sk.MouseX() >= x1 && sk.MouseX() <= x2 && sk.MouseY() >= y1 && sk.MouseY() <= y2 && 
                sk.MouseClicked(MouseButton.LeftButton))
            {
                return true;
            }
            return false;
        }
    }
}
