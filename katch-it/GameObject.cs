using System;
using SplashKitSDK;
using sk = SplashKitSDK.SplashKit;

namespace GameOfGrades {

    public abstract class GameObject {

        private float _x;
        private float _y;
        private float _dx;
        private float _dy;
        private Bitmap _bmp;
        private Game _world;
        private int _score;

        public GameObject( Game world ) {
            _world = world;
        }

        public abstract void Update();

        public virtual void Render() {
            sk.DrawBitmap( _bmp, _x, _y );
        }

        public Bitmap Bitmap {
            get { return _bmp; }
            set { _bmp = value; }
        }

        public float X {
            get { return _x; }
            set { _x = value; }
        }

        public float Y {
            get { return _y; }
            set { _y = value; }
        }

        public float DX {
            get { return _dx; }
            set { _dx = value; }
        }

        public float DY {
            get { return _dy; }
            set { _dy = value; }
        }

        public Game World {
            get { return _world; }
        }

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }
    }
}