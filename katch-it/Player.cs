using System;
using System.Collections.Generic;
using SplashKitSDK;
using sk = SplashKitSDK.SplashKit;

namespace GameOfGrades {

    public enum Catcher
    {
        basket,
        player
    }

    public class Player : GameObject {

        private const int STARTPOS = 470;
        private const int H_SPEED = 5;
        //private const int V_SPEED = 0;

        public Player( Game world ) : base( world ) {
            Bitmap = sk.LoadBitmap( "player", "player.png" );
            X = ( 400 - sk.BitmapWidth( Bitmap ) ) / 2;
            Y = STARTPOS;
            DX = H_SPEED;
            Score = 0;
            //DY = V_SPEED;
        }

        public override void Update() {
            if ( (sk.KeyDown( KeyCode.AKey ) || sk.KeyDown( KeyCode.LeftKey )) && X > 0 ) {
                X -= DX;
            }
            if ( ( sk.KeyDown( KeyCode.DKey ) || sk.KeyDown( KeyCode.RightKey ) ) && X < ( 400 - sk.BitmapWidth( Bitmap ) ) ) {
                X += DX;
            }
        }

        public void Change()
        {
            sk.LoadBitmap(Catcher.player.ToString(), Catcher.player + ".png");
        }
    }
}