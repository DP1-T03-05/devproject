# Meeting Minutes
**Date:** 2/10/2017
**Start Time:** 2:59PM
**End Time:** 3:19PM
**Location:** Swinburne EN310
**Attendees:** Endru Joshua Hosmar, Peggy Liu, Joseph Slingo

---
# Agenda Items

+ User Stories Storage
	+ Decision/Outcome: GDD document
	+ Action: All
	
+ Product Backlog Storage
	+ Decision/Outcome: Trello, new collumn
	+ Action: Peggy
	
+ Task Title or Description
	+ Decision/Outcome: Description
	+ Action: All

+ Testing tool (Unit Tests)
	+ Decision/Outcome: MSTest
	+ Action: All
	
+ Upload meeting minutes and where
	+ Decision/Outcome: Repository, make seperate folder
	+ Action: Joseph
	
+ How will Slack be used
	+ Decision/Outcome: Informal communication, brainstorming
	+ Action: All
	
+ Game sketches storage/who
	+ Decision/Outcome: GDD
	+ Action: Endru
	
+ Code review. When?
	+ Decision/Outcome: 29/09/17
	+ Action: All
	
+ Estimation system (time, size, etc.)
	+ Decision/Outcome: Time in hours
	+ Action: All

+ Sprint details storage
	+ Decision/Outcome: Trello new collumn
	+ Action: Peggy

---
