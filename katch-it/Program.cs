using System;
using SplashKitSDK;

namespace GameOfGrades {

    public class Program {

        public static void Main() {
            Game game = new Game();
            game.Start();
        }
    }
}