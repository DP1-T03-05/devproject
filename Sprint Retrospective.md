#Katch-It Sprint Retrospective
Team Member: Peiqi liu
             Joseph Slingo
             Endru Joshua Hosmar
			  
## What Worked Well

+ **Extra meeting**
	We met up outside of class time inorder to catch-up.
	
+ **List clarity**
	We know exactly what to do from the highly descriptive list we
	have created.
	
+ **Decision Making**
	Didn't spend too long on small decisions and got straight to the point.
	
+ **Hard Working**
	Put extra effort into the work.

+ **Team Spirit**
	Happy to help each other and get along well.
	
	
## Improvements

+ **Putting it into practice**
	Put off doing the actual coding.
	Improve motivation.
	
+ **Communications**
	Not as much communication between ourselves as we should outside of class. 
	Keep communication windows open to help each other.
	
+ **Repository Updates**
	Repository not updates as often as it should.
	Update the repository more often.
	